# MyBowtie Theme Template

## Getting Started

	npm install
	gulp
	open http://localhost:5000

## theme.json

Your theme is defined in `theme.json`. This is where you name it and describe it. You are also able to decide whether or not it is a public theme.
You are able to change this data after uploading the theme aswell.

## Default Setup

The repo is setup to make gulp perform a lot of convenient tasks by default. Gulp will start a webserver, compile stylus, watch for changes and also
package the zip file when you are feeling up for it.

The development process is entirely up to you. See `gulpfile.js` for tasks and [http://gulpjs.com/](gulp docs) for more information.

## Available Data

The file `templates/data.json` represents a data object that theme developer can expect to receive from the server. That data is fetched straight from <a href="http://api.bowtie.dev/v1/themes/sampledata">here</a>.
Do not modify that file.

## Issues and Suggestions

Please post anything related to the template in the [https://bitbucket.org/mybowtie/theme-template/issues/new](issue tracker).
